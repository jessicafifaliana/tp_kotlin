package entities

class Library {
    var books = mutableListOf<Book>();

    fun addBook(book:Book){
        books.add(book)
    }

    fun displayBooks(){
        for(book in books){
            println("[LIVRE] "+book.toJSON())
        }
    }

    fun deleteBookById(id : String){
        val book: Book? = books.find { it.id == id }
        books.remove(book);
    }
}