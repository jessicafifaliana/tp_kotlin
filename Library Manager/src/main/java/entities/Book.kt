package entities

import com.google.gson.Gson

class Book(author:String,title:String,genre:String) {
    val id :String = java.util.UUID.randomUUID().toString();
    var author=author;
    var title=title;
    var genre=genre;

    fun display(): String {
        return "Id : "+id+"/ Titre : "+title+" / Auteur : "+author+" / Genre : "+genre;
    }

    fun toJSON(): String? {
        val gson= Gson()
        return gson.toJson(this)
    }
}
