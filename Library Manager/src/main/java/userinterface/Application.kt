package userinterface

import entities.Book
import entities.Library
import external.Export
import usecases.Management

fun main(args: Array<String>) {
 var library=Library()
    var export = Export()
    menu(library,export);
}
fun menu(library: Library,export:Export){

    var m = Management(library,export)
    while(true) {
        println("Que souhaitez-vous faire ?");
        println("1- Ajouter un livre");
        println("2- Lister les livres");
        println("3- Supprimer un livre");
        println("4- Compter les livres");
        println("5- Exporter les livres");

        when (readLine()!!) {
            "1" -> m.addBookToLibrary()
            "2" -> m.getBooks()
            "3" -> m.deleteBookById()
            "4" -> m.countBooks()
            "5" -> m.exportBooks()
            else -> {
                println("Veuillez choisir entre 1 et 5")
            }
        }
    }
}