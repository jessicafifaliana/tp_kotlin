package external

import com.google.gson.Gson
import entities.Library
import entities.ExportInterface
import java.io.File
import java.time.LocalDateTime

class Export: ExportInterface {
    override fun exportLibrary(library: Library) {
        val current = LocalDateTime.now()
        val year =current.year
        val month =current.month
        val day = current.dayOfMonth
        val hour=current.hour
        val min=current.minute

        val books =library.books
        val gson=Gson()
        val books_json= gson.toJson(books)
        val fileName="exports/export_"+day+month+year+"_"+hour+"h"+min+".txt"
        var file = File(fileName)

        val isFileCreated :Boolean = file.createNewFile()

        if(isFileCreated){
            file.printWriter().use { out ->
               out.print(books_json)
            }
            println("Les livres ont été exportés dans le dossiers exports")
        } else{
            println("Erreur lors de l'import")
        }
    }
}

