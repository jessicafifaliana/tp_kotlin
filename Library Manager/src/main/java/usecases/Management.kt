package usecases

import entities.Book
import entities.ExportInterface
import entities.Library

class Management(library: Library,exportInterface: ExportInterface) {
    var library = library
    var exportInterface =exportInterface
    fun addBookToLibrary(){
        println("Bienvenue dans votre bibliothèque. Pour ajouter un livre, appuyez sur entrer pour continuer");
        System.`in`.read()
        println("Auteur :");
        val author = readLine()!!
        println("Titre :");
        val title = readLine()!!
        println("Genre :");
        val genre = readLine()!!

        var book= Book(author, title, genre);
        library.addBook(book);
    }
    fun getBooks(){
        println("La bibliothèque contient les livres suivants : ");
        library.displayBooks();
    }
    fun deleteBookById(){
        println("Entrer l'identifiant du livre à supprimer ");
        val id = readLine()!!
        println("Le livre a été effacé de la bibliothèque ");
        library.deleteBookById(id);
    }

    fun countBooks(){
        println("Il y a "+library.books.size+" livres dans la bibliothèque")

    }

    fun exportBooks() {
        exportInterface.exportLibrary(library)
    }
}