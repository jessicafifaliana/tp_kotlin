package entities

import com.tngtech.archunit.core.domain.JavaClasses
import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition



@AnalyzeClasses(packagesOf = [ArchTestEntities::class])
class ArchTestEntities {

    @ArchTest
    fun if_dont_depend_on_any(importedClasses: JavaClasses){
        val rule1 = ArchRuleDefinition.noClasses().that().resideInAPackage("..entities..").should().dependOnClassesThat().resideInAnyPackage("..external..","..usecases..","..userinterface..")
        rule1.check(importedClasses)

    }
}