package usecases

import com.tngtech.archunit.core.domain.JavaClasses
import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition
import entities.ArchTestEntities

@AnalyzeClasses(packagesOf = [ArchTestUC::class])
class ArchTestUC {
    @ArchTest
    fun if_depend_on_other_than_entities(importedClasses: JavaClasses){
        val rule2 = ArchRuleDefinition.noClasses().that().resideInAPackage("..usecases..").should().dependOnClassesThat().resideInAnyPackage("..external..","..userinterface..")
        rule2.check(importedClasses)
    }
}